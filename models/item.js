const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
    item_name: {
        type: String,
        required: 'Item name is required',
        max: 100,
        trim: true
    },
    item_price: {
        type: Number,
        required: 'Item price is required',
        min:1,
        max: 10000
    },
    item_brand: {
        type:String,
        required: 'Item brand is required',
    },
    item_image_string: {
        type: String,
        required: 'Item image is required'
    },
    item_image_string_arr: {
        type: Array
    },
    item_description: {
        type: String,
        required: 'Item description is required'
    },
    item_warranty: {
        type: Number,
    },
    item_available: {
        type: Boolean,
        required: 'Availability is required'
    },
    item_category_tear1: {
        type: String,
        required: 'Item category is required',
        trim: true
    },
});

module.exports = mongoose.model('Item', itemSchema);