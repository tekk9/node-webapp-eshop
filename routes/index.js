var express = require('express');
var router = express.Router();

const eShopController = require('../controllers/eShopController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/item', eShopController.itemDetails);
router.get('/item/:item', eShopController.itemDetails);

//Admin Routes:
router.get('/admin', eShopController.adminPage);
// router.get('/admin/add', eShopController.createItemGet);

module.exports = router;
